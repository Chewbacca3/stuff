
#include <Servo.h> 
#include <stdint.h>

int pinLB=14;     // Define a 14 Pin
int pinLF=15;     // Define a 15 Pin

int pinRB=17;    // Define a 16 Pin
int pinRF=16;    // Define a 17 Pin

//int MotorLPWM=5;  //Define a 5 Pin
//int MotorRPWM=6;  //Define a 6 Pin

int inputPin = 9;  // Define the ultrasound signal receiving a Pin
int outputPin = 8;  //Define the ultrasound signal emission Pin


Servo myservo;        // Set up the myservo


constexpr const size_t kServoMovementShortDelay = 250;                // Servo motion settle time in milliseconds
constexpr const size_t kServoMovementDelay = 350;                // Servo motion settle time in milliseconds

enum tMotorState
{
    eMotorStop,
    eMotorForward,
    eMotorBack,
    eMotorRight,
    eMotorLeft
};

tMotorState motorState = eMotorStop;


void setup()
{
    Serial.begin(9600);     // Initialize 
    pinMode(pinLB,OUTPUT); // Define 14 pin for the output (PWM)
    pinMode(pinLF,OUTPUT); // Define 15 pin for the output (PWM)
    pinMode(pinRB,OUTPUT); // Define 16 pin for the output (PWM) 
    pinMode(pinRF,OUTPUT); // Define 17 pin for the output (PWM)
    
    //pinMode(MotorLPWM,  OUTPUT);  // Define 5 pin for PWM output 
   // pinMode(MotorRPWM,  OUTPUT);  // Define 6 pin for PWM output
    
    pinMode(inputPin, INPUT);    // Define the ultrasound enter pin
    pinMode(outputPin, OUTPUT);  // Define the ultrasound output pin   
      
    myservo.attach(11);    // Define the servo motor output 10 pin(PWM)
    myservo.write(90);
    delay(3000);
    
}

size_t MeasureUltrasonicDistance()
{
    constexpr const size_t kMaxUltrasonicCm = 30;           // Number of microseconds per centimeter
    constexpr const size_t kUsPerCm = 59;                   // Number of microseconds per centimeter
    constexpr const size_t kMaxUltrasonicUs = kMaxUltrasonicCm * kUsPerCm; // Number of microseconds per centimeter
    constexpr const size_t kUltrasonicNumerator = 35;       // Numerator to multiply by to convert microseconds per centimeter
    constexpr const size_t kUltrasonicDenominator = 2048;   // Denominator to divide by to convert microseconds per centimeter
    
//    static_assert(((uint32_t)kMaxUltrasonicUs * (uint32_t)kUltrasonicNumerator) <= (uint32_t)SIZE_MAX);

    digitalWrite(outputPin, LOW);   // Set low for 2 uS to prepare for ultrasonic measuer
    delayMicroseconds(2);
    digitalWrite(outputPin, HIGH);  // Set high for 10 uS to trigger ultrasonic measuer
    delayMicroseconds(10);
    digitalWrite(outputPin, LOW);    // Set low to finish trigger

    size_t pingTime = pulseIn(inputPin, HIGH);  // Measure the ping time in uS
    
    // If ping is longer than the max distance we are measuring or is zero, set to
    // max allowed.  This will prevent integer variable overflow in the math below
    if ((pingTime > kMaxUltrasonicUs) || (pingTime == 0))
    {
        pingTime = kMaxUltrasonicUs;
    }
    
    // The microsecond to centimeter conversion is done by multipling by a number and then
    // dividing by a bigger number that happens to be a power of 2 (ie 2 to the N power).
    // The reason to do this is multipling by odd numbers is much faster on the CPU than divide.
    // The divide by a power of 2 is very fast.
    size_t distanceMeasure = pingTime * kUltrasonicNumerator;
    distanceMeasure = distanceMeasure / kUltrasonicDenominator;

    return distanceMeasure;              
}

size_t MeasureDistance(size_t angle)   // Measure the distance ahead
{
    myservo.write(angle);
    delay(kServoMovementDelay);
    return MeasureUltrasonicDistance();
}  

size_t MeasureForward()   // Measure the distance ahead
{
    return MeasureDistance(90);
}  

size_t MeasureLeft()   // Measure the distance on the left 
{
    return MeasureDistance(55);
}  
size_t MeasureRight()   // Measure the distance on the right 
{
    return MeasureDistance(125);
}  
    

void WaitForTime(unsigned long startTime, unsigned long waitTime)
{
    unsigned long currentTime;
    do
    {
        currentTime = millis();
    }
    while ((currentTime - startTime) < waitTime);
}

void Forward()     // go
{
    motorState = eMotorForward;
    digitalWrite(pinRB,HIGH);  // 16 feet for high level
    digitalWrite(pinRF,LOW);   //17 feet for low level
    digitalWrite(pinLB,HIGH);  // 14 feet for high level
    digitalWrite(pinLF,LOW);   //15 feet for high level
}

void ForwardRight(void)        //right
{
    motorState = eMotorRight;
    digitalWrite(pinRB,LOW);   
    digitalWrite(pinRF,LOW);
    digitalWrite(pinLB,HIGH);
    digitalWrite(pinLF,LOW);
}

void ForwardLeft(void)         //left
{
    motorState = eMotorLeft;
    digitalWrite(pinRB,HIGH);
    digitalWrite(pinRF,LOW);
    digitalWrite(pinLB,LOW);   
    digitalWrite(pinLF,LOW);
}

void SpinRight(void)        //right
{
    motorState = eMotorRight;
    digitalWrite(pinRB,HIGH);  
    digitalWrite(pinRF,LOW);
    digitalWrite(pinLB,LOW);
    digitalWrite(pinLF,HIGH);  
}

void SpinLeft(void)        //left
{
    motorState = eMotorLeft;
    digitalWrite(pinRB,LOW);
    digitalWrite(pinRF,HIGH);   
    digitalWrite(pinLB,HIGH);   
    digitalWrite(pinLF,LOW);
}    

void BackRight(void)        //right
{
    motorState = eMotorRight;
    digitalWrite(pinRB,LOW);  
    digitalWrite(pinRF,LOW);
    digitalWrite(pinLB,LOW);
    digitalWrite(pinLF,HIGH);  
}

void BackLeft(void)        //left
{
    motorState = eMotorLeft;
    digitalWrite(pinRB,LOW);
    digitalWrite(pinRF,HIGH);   
    digitalWrite(pinLB,LOW);   
    digitalWrite(pinLF,LOW);
}    

void Stop(void)         //stop
{
    motorState = eMotorStop;
    digitalWrite(pinRB,LOW);
    digitalWrite(pinRF,LOW);
    digitalWrite(pinLB,LOW);
    digitalWrite(pinLF,LOW);
}

void Back(void)          //Back
{
    motorState = eMotorBack;
    digitalWrite(pinRB,LOW);  
    digitalWrite(pinRF,HIGH);
    digitalWrite(pinLB,LOW);  
    digitalWrite(pinLF,HIGH);
}
    
void detection(void)        //Measuring three angles(0.90.179)
{      
    constexpr const size_t kLeftSlightly = 65;
    constexpr const size_t kRightSlightly = 115;

    constexpr const size_t kLeft = 25;

    size_t frontLeftDistance;
    size_t frontRightDistance;

    if (motorState == eMotorForward)
    {
        // Measure in front of us, a little to the left
        frontLeftDistance = MeasureDistance(kLeftSlightly);

        if (frontLeftDistance < 25)
        {
            Stop();
            Serial.print("\nFrontLeft ");
            Serial.print(frontLeftDistance);
        }
        // {
        //     if (frontLeftDistance < 10)
        //     {
        //         Stop();
        //     }
        //     else
        //     {
        //         // We see something but not super close. Try to turn away from it
        //         unsigned long startTime = millis();
        //         ForwardRight();

        //         frontRightDistance = MeasureDistance(kRightSlightly);
                
        //         if (frontRightDistance < 25)
        //         {
        //             Stop();
        //         }
        //         else
        //         {
        //             WaitForTime(startTime, 1000);
        //             frontLeftDistance = MeasureDistance(kLeftSlightly);
                    
        //             if (frontLeftDistance < 25)
        //             {
        //                 Stop();
        //             }
        //             else
        //             {
        //                 Forward();
        //             }
        //         }
        //     }
        // }
    }

    if (motorState == eMotorForward)
    {
        // Measure in front of us, a little to the right
        frontRightDistance = MeasureDistance(kRightSlightly);

        if (frontRightDistance < 25)
        {
            Stop();
            Serial.print("\nFrontRight ");
            Serial.print(frontRightDistance);
        }
        // {
        //     if (frontRightDistance < 10)
        //     {
        //         Stop();
        //     }
        //     else
        //     {
        //         // We see something but not super close. Try to turn away from it
        //         unsigned long startTime = millis();
        //         ForwardLeft();

        //         frontLeftDistance = MeasureDistance(kLeftSlightly);
                
        //         if (frontLeftDistance < 25)
        //         {
        //             Stop();
        //         }
        //         else
        //         {
        //             WaitForTime(startTime, 1000);
        //             frontRightDistance = MeasureDistance(kRightSlightly);
                    
        //             if (frontRightDistance < 25)
        //             {
        //                 Stop();
        //             }
        //             else
        //             {
        //                 Forward();
        //             }
        //         }
        //     }
        // }
    }
    
    if (motorState == eMotorStop)
    {
        size_t leftDistance = MeasureLeft();            // Read the left distance
        size_t rightDistance = MeasureRight();            // Read the right distance  
        bool goRight = false;

        Serial.print("\nStopped, scan ");
        Serial.print(leftDistance);
        Serial.print(" ");
        Serial.print(rightDistance);
    
        // If the left side is closer, go to the right.
        if(leftDistance < rightDistance)
        {
            goRight = true;      
        }

        if ((leftDistance < 10) && (rightDistance < 10))   /*If the left front distance and distance and the right distance is less than 15 cm */
        {
            Serial.print("\nGo Back ");
            Back();
            delay(1000);
            Stop();
        }
        else if ((leftDistance < 25) && (rightDistance < 25))
        {
            if (goRight == true)
            {
                Serial.print("\nSpin Right ");
                Serial.print(rightDistance);
                SpinRight();
            }
            else
            {
                Serial.print("\nSpin Left ");
                Serial.print(leftDistance);
                SpinLeft();
            }

            delay(500);
            Stop();
        }
        else
        {
            Serial.print("\nWas stop, now forward ");
            Forward();
        }
    }
}    



void loop()
{
    detection();        //Measuring Angle And determine which direction to go to
}

