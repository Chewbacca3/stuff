
//#include <Servo.h> 
#include <stdint.h>

int pinLB=15;     // Define a 14 Pin
int pinLF=14;     // Define a 15 Pin

int pinRB=17;    // Define a 16 Pin
int pinRF=16;    // Define a 17 Pin

int inputPin = 9;  // Define the ultrasound signal receiving a Pin
int outputPin = 8;  //Define the ultrasound signal emission Pin

int irLeftPin = 5;  //Define the ultrasound signal emission Pin
int ircenterPin = 6;  //Define the ultrasound signal emission Pin
int irRightPin = 7;  //Define the ultrasound signal emission Pin

unsigned long runTime = 0;

enum tMotorState
{
    eMotorStop,
    eMotorForward,
    eMotorBack,
    eMotorRight,
    eMotorLeft
};

tMotorState motorState = eMotorStop;


void setup()
{
    Serial.begin(9600);     // Initialize 

    pinMode(pinLB,OUTPUT); // Define 14 pin for the output (PWM)
    pinMode(pinLF,OUTPUT); // Define 15 pin for the output (PWM)
    pinMode(pinRB,OUTPUT); // Define 16 pin for the output (PWM) 
    pinMode(pinRF,OUTPUT); // Define 17 pin for the output (PWM)
    
    Stop();
  
    pinMode(irLeftPin, INPUT);    // Define the ultrasound enter pin
    pinMode(ircenterPin, INPUT);    // Define the ultrasound enter pin
    pinMode(irRightPin, INPUT);    // Define the ultrasound enter pin
    
    Serial.print("Hello People!\n");
    runTime = millis();

    delay(3000);
    
}


void WaitForTime(unsigned long startTime, unsigned long waitTime)
{
    unsigned long currentTime;
    do
    {
        currentTime = millis();
    }
    while ((currentTime - startTime) < waitTime);
}

bool HasTimeExpired(unsigned long startTime, unsigned long waitTime)
{
    unsigned long currentTime = millis();

    return ((currentTime - startTime) < waitTime) ? false : true;
}

void Forward()     // go
{
    if (motorState != eMotorForward)
    {
        Serial.print("Going forward from ");
        Serial.print(motorState);
        Serial.print("\n");
    }
    motorState = eMotorForward;
    digitalWrite(pinRB,HIGH);  // 16 feet for high level
    digitalWrite(pinRF,LOW);   //17 feet for low level
    digitalWrite(pinLB,HIGH);  // 14 feet for high level
    digitalWrite(pinLF,LOW);   //15 feet for high level
}

void ForwardRight(void)        //right
{
    motorState = eMotorRight;
    digitalWrite(pinRB,LOW);   
    digitalWrite(pinRF,LOW);
    digitalWrite(pinLB,HIGH);
    digitalWrite(pinLF,LOW);
}

void ForwardLeft(void)         //left
{
    motorState = eMotorLeft;
    digitalWrite(pinRB,HIGH);
    digitalWrite(pinRF,LOW);
    digitalWrite(pinLB,LOW);   
    digitalWrite(pinLF,LOW);
}

void SpinRight(void)        //right
{
    motorState = eMotorRight;
    digitalWrite(pinRB,HIGH);  
    digitalWrite(pinRF,LOW);
    digitalWrite(pinLB,LOW);
    digitalWrite(pinLF,HIGH);  
}

void SpinLeft(void)        //left
{
    motorState = eMotorLeft;
    digitalWrite(pinRB,LOW);
    digitalWrite(pinRF,HIGH);   
    digitalWrite(pinLB,HIGH);   
    digitalWrite(pinLF,LOW);
}    

void BackRight(void)        //right
{
    motorState = eMotorRight;
    digitalWrite(pinRB,LOW);  
    digitalWrite(pinRF,LOW);
    digitalWrite(pinLB,LOW);
    digitalWrite(pinLF,HIGH);  
}

void BackLeft(void)        //left
{
    motorState = eMotorLeft;
    digitalWrite(pinRB,LOW);
    digitalWrite(pinRF,HIGH);   
    digitalWrite(pinLB,LOW);   
    digitalWrite(pinLF,LOW);
}    

void Stop(void)         //stop
{
    motorState = eMotorStop;
    digitalWrite(pinRB,LOW);
    digitalWrite(pinRF,LOW);
    digitalWrite(pinLB,LOW);
    digitalWrite(pinLF,LOW);
}

void Back(void)          //Back
{
    motorState = eMotorBack;
    digitalWrite(pinRB,LOW);  
    digitalWrite(pinRF,HIGH);
    digitalWrite(pinLB,LOW);  
    digitalWrite(pinLF,HIGH);
}
    

void PrintState(void)
{
    int left;
    int center;
    int right;
    static unsigned long printTime;

    if (HasTimeExpired(printTime, 2000) == false)
    {
        return;
    }

    printTime = millis();

    left = readIrSensor(irLeftPin);
    center = readIrSensor(ircenterPin);
    right = readIrSensor(irRightPin);

    if (left == HIGH)
    {
        Serial.print("Left HIGH, ");
    }
    else
    {
        Serial.print("Left LOW, ");
    }

    if (center == HIGH)
    {
        Serial.print("Center HIGH, ");
    }
    else
    {
        Serial.print("Center LOW, ");
    }

    if (right == HIGH)
    {
        Serial.print("Right HIGH\n");
    }
    else
    {
        Serial.print("Right LOW\n");
    }

}

int readIrSensor(int pin)
{
    int val = digitalRead(pin);
    char debounceCount = 0;
    char tryCount = 0; 

    while (debounceCount < 4)
    {
        int newVal = digitalRead(pin);
        tryCount++;

        if (newVal == val)
        {
            debounceCount++;
        }
        else
        {
            val = newVal;
        }

        if (tryCount > 10)
        {
            Serial.print("Debounce error on pin ");
            Serial.print(pin);
            Serial.print("\n");
            debounceCount = tryCount;
        }
    }

    if (tryCount != debounceCount)
    {
        Serial.print("Debounce mismatch on pin ");
        Serial.print(pin);
        Serial.print("\n");
    }

    if (val == LOW)
    {
        return HIGH;
    }
    else
    {
        return LOW;
    }
}
const int backupTime = 275;
const int spinTime = 175;

void loop()
{
    int left = readIrSensor(irLeftPin);
    int center = readIrSensor(ircenterPin);
    int right = readIrSensor(irRightPin);
    unsigned long startTime;

    PrintState();

    if ((left == HIGH) || (center == HIGH))
    {
        Back();
        Serial.print("Front or left\n");
        delay(backupTime);
        SpinRight();
        delay(spinTime);

    }
    else if (right == HIGH)
    {
        Back();
        Serial.print("Right\n");
        delay(backupTime);
        SpinLeft();
        delay(spinTime);
    }

    Forward();
}

