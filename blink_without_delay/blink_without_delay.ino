/*
  Blink without Delay

  Turns on and off a light emitting diode (LED) connected to a digital pin,
  without using the delay() function. This means that other code can run at the
  same time without being interrupted by the LED code.

  The circuit:
  - Use the onboard LED.
  - Note: Most Arduinos have an on-board LED you can control. On the UNO, MEGA
    and ZERO it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN
    is set to the correct LED pin independent of which board is used.
    If you want to know what pin the on-board LED is connected to on your
    Arduino model, check the Technical Specs of your board at:
    https://www.arduino.cc/en/Main/Products

  created 2005
  by David A. Mellis
  modified 8 Feb 2010
  by Paul Stoffregen
  modified 11 Nov 2013
  by Scott Fitzgerald
  modified 9 Jan 2017
  by Arturo Guadalupi

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/BlinkWithoutDelay
*/
#include <Servo.h>


void forward();
 
// constants won't change. Used here to set a pin number:
const int ledPin =  LED_BUILTIN;// the number of the LED pin
Servo myservo;
Servo rightWheel;
Servo leftWheel; 
int rightStop = 82;
int leftStop = 85;


// Variables will change:
int ledState = LOW;             // ledState used to set the LED
int count = 2;
int endingpoint;
// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0;        // will store last time LED was updated

long interval = 100;           // interval at which to blink (milliseconds)
bool up = true;
int mult_val = 5;
  int val = 50;

void setup() {
  // set the digital pin as output:
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
  endingpoint = random(2,12);
 myservo.attach(10);
 rightWheel.attach(9);
 rightWheel.write(rightStop);
 leftWheel.attach(8);
 leftWheel.write(leftStop);
    rightWheel.write(rightStop);
    leftWheel.write(leftStop);
}

void loop() {
  // here is where you'd put code that needs to be running all the time.





  // check to see if it's time to blink the LED; that is, if the difference
  // between the current time and last time you blinked the LED is bigger than
  // the interval at which you want to blink the LED.
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;

 
 /*     if (up == true) {
        val += mult_val;
        if (val > 140) {
          up = false;
        }
      }
      else {
        val -= mult_val;
        if (val < 30) {
          up = true;
        }
      } */


int base_val = 30;
char a = Serial.read();

if (a == 'w') {
    rightWheel.write(180);
    leftWheel.write(0);
}

if (a == 's') {
    rightWheel.write(0);
    leftWheel.write(180);
}
if (a == ' ') {
    rightWheel.write(rightStop);
    leftWheel.write(leftStop);
}

if (a == 'd') {
    rightWheel.write(180);
    leftWheel.write(180);
}

if (a == 'a') {
    rightWheel.write(0);
    leftWheel.write(0);
}

if (a == 'q') {
    val = 70;
}

if (a == 'e') {
    val = 90;
}

if (a == '+') {
    rightStop += 1;
    Serial.println(rightStop);
    rightWheel.write(rightStop);
    leftWheel.write(leftStop);
}
if (a == '-') {
    rightStop -= 1;
    Serial.println(rightStop);
    rightWheel.write(rightStop);
    leftWheel.write(leftStop);
}
if (a == ']') {
    leftStop += 1;
    Serial.println(leftStop);
    rightWheel.write(rightStop);
    leftWheel.write(leftStop);
}
if (a == '[') {
    leftStop -= 1;
    Serial.println(leftStop);
    rightWheel.write(rightStop);
    leftWheel.write(leftStop);
}
if (a == 'b') {
    rightWheel.write(180);
    leftWheel.write(0);
}
if (a == 'v') {
    rightWheel.write(rightStop);
    leftWheel.write(leftStop);
}

    myservo.write(val);
 
      // if the LED is off turn it on and vice-versa:
      if (ledState == LOW) {
        ledState = HIGH;
      } else {
        ledState = LOW;
      }
    }
}


void forward()
{
rightWheel.write(180);
leftWheel.write(0);
}

void left()
{    
  rightWheel.write(0);
  leftWheel.write(0);
}

void right()
{
    rightWheel.write(180);
    leftWheel.write(180);
}

void backwards()
{ 
    rightWheel.write(0);
    leftWheel.write(180);
}




